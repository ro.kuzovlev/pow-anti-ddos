package requests

import (
	"bufio"
	"ddos/client/hashcash"
	"fmt"
	"net"
	"strings"
)

// GetWiseQuote tries to get a quote from the target server after requesting and solving a challenge.
func GetWiseQuote(serverAddr string) (string, error) {
	conn, err := net.Dial("tcp", serverAddr)
	if err != nil {
		return "", fmt.Errorf("error connecting to server: %w", err)
	}
	defer conn.Close()

	// receive the challenge from the server
	challenge := make([]byte, 33) // buffer size matches the challenge size
	_, err = conn.Read(challenge)
	if err != nil {
		return "", fmt.Errorf("error reading challenge: %w", err)
	}
	// generate a solution
	solution, err := hashcash.GenerateSolution(challenge)
	if err != nil {
		return "", fmt.Errorf("error solving challenge: %w", err)
	}
	// send the solution to the server
	_, err = conn.Write(solution)
	if err != nil {
		return "", fmt.Errorf("error sending solution: %w", err)
	}
	// get the quote from the server
	reader := bufio.NewReader(conn)
	quote, err := reader.ReadString('\n')
	if err != nil {
		return "", fmt.Errorf("error reading quote from server: %w", err)
	}

	return strings.TrimSuffix(quote, "\n"), nil

}
