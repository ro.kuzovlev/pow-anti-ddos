package hashcash

import (
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"strconv"
	"strings"
)

// GenerateSolution generates a solution for a given challenge by trying different nonces.
func GenerateSolution(challenge []byte) ([]byte, error) {
	complexity, err := strconv.Atoi(string(challenge[0])) // first byte is the complexity level
	if err != nil {
		return nil, fmt.Errorf("error converting complexity: %w", err)
	}
	prefix := challenge[1:] // actual challenge bytes

	nonce := 0
	for {
		// generate a solution to be tried
		candidate := append(prefix, []byte(fmt.Sprintf("%d", nonce))...)
		hash := sha256.Sum256(candidate)

		// check if it's a correct solution
		if checkLeadingZeros(hash[:], complexity) {
			// if yes, return it
			return []byte(fmt.Sprintf("%d", nonce)), nil
		}
		nonce++
	}
}

func checkLeadingZeros(hash []byte, complexity int) bool {
	hashString := hex.EncodeToString(hash)
	prefix := strings.Repeat("0", complexity)
	return strings.HasPrefix(hashString, prefix)
}
