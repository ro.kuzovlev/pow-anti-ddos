package main

import (
	"ddos/client/requests"
	"fmt"
	"os"
	"os/signal"
	"strconv"
	"syscall"
	"time"

	"github.com/joho/godotenv"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		fmt.Println("Error loading .env")
		return
	}

	port := getEnv("PORT", "8080")
	targetRate, _ := strconv.Atoi(getEnv("TARGET_RATE", "10"))

	var addr string
	if _, exists := os.LookupEnv("DOCKER_ENV"); exists {
		addr = "host.docker.internal:" + port
	} else {
		addr = "localhost:" + port
	}
	// for a graceful shutdown
	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, os.Interrupt, syscall.SIGTERM)

	successfulRequests := 0
	running := true
	for running {
		start := time.Now()
		successfulRequests = 0

		for time.Since(start) < time.Second && running {
			select {
			case <-sigChan:
				fmt.Println("\nGraceful shutdown")
				running = false
				continue
			default:
				quote, err := requests.GetWiseQuote(addr)
				if err != nil {
					fmt.Println("Request failed:", err)
					return
				}
				fmt.Println("Quote received: ", quote) // comment this line to make logs cleaner
				successfulRequests++
			}
		}

		fmt.Printf("Requests made this second: %d\n", successfulRequests)
		if successfulRequests < targetRate && running {
			fmt.Println("Cannot make more than 10 requests per second anymore, stopping in disappointment. DDOS attack is mitigated.")
			break
		}
	}
}

func getEnv(key, defaultValue string) string {
	if value, exists := os.LookupEnv(key); exists {
		return value
	}
	return defaultValue
}
