package quotes

import (
	mrand "math/rand"
	"time"
)

// GetRandomQuote returns a single random quote
func GetRandomQuote() string {
	mrand.Seed(time.Now().UnixNano())
	index := mrand.Intn(len(quotes))
	return quotes[index]
}

// quotes from famous computer scientists
var quotes = []string{
	"It's easier to ask forgiveness than it is to get permission. - Grace Hopper",
	"Premature optimization is the root of all evil. - Donald Knuth",
	"A computer is like a mischievous genie. It will give you exactly what you ask for, but not always what you want. - Joe Sondow",
	"The question of whether a computer can think is no more interesting than the question of whether a submarine can swim. - Edsger W. Dijkstra",
	"The most disastrous thing that you can ever learn is your first programming language. - Alan Kay",
	"The best way to predict the future is to implement it. - David Heinemeier Hansson",
	"One of my most productive days was throwing away 1000 lines of code. - Ken Thompson",
	"Always code as if the guy who ends up maintaining your code will be a violent psychopath who knows where you live. - John Woods",
	"Talk is cheap. Show me the code. - Linus Torvalds",
	"Programs must be written for people to read, and only incidentally for machines to execute. - Harold Abelson",
}
