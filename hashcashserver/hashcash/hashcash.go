package hashcash

import (
	"crypto/rand"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"strconv"
	"strings"
)

// ChallengeMaker creates challenges
type ChallengeMaker struct{}

// MakeChallenge creates a challenge with a given complexity
func (rcm *ChallengeMaker) MakeChallenge(complexity int) ([]byte, error) {
	if complexity < 1 || complexity > 9 {
		return nil, fmt.Errorf("complexity should be between 1 and 9")
	}

	// the first byte is for complexity level + 32 bytes for an input for hashing
	challengeBytes := make([]byte, 33)
	_, err := rand.Read(challengeBytes) // fill with random bytes
	if err != nil {
		return nil, fmt.Errorf("error while generating challengeBytes: %w", err)
	}

	// encode the complexity level in the leading byte
	challengeBytes[0] = []byte(strconv.Itoa(complexity))[0]

	return challengeBytes, nil
}

// SolutionChecker checks solutions
type SolutionChecker struct{}

// CheckSolution check if the solution for a given challenge is
func (rsc *SolutionChecker) CheckSolution(challenge, solution []byte) bool {
	solution = trimTrailingZeros(solution)                // because we create a buffer; may be optimized
	complexity, err := strconv.Atoi(string(challenge[0])) // convert byte to int
	if err != nil {
		return false
	}
	prefix := challenge[1:] // actual random bytes for the hash input (first byte is a complexity level)

	// recreate hash by using actual input + solution nonce
	candidate := append(prefix, solution...)
	hash := sha256.Sum256(candidate)

	// check if the hash meets the complexity requirement
	return checkLeadingZeros(hash[:], complexity)
}

// this function may be improved by checking in []bytes without a convertation to a string
func checkLeadingZeros(hash []byte, complexity int) bool {
	hashString := hex.EncodeToString(hash)
	prefix := strings.Repeat("0", complexity)
	return strings.HasPrefix(hashString, prefix)
}

func trimTrailingZeros(data []byte) []byte {
	// find the index of the last non-zero byte
	lastIndex := -1
	for i, b := range data {
		if b != 0 {
			lastIndex = i
		}
	}
	return data[:lastIndex+1]
}
