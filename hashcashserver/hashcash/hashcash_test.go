package hashcash

import (
	"strconv"
	"testing"
)

func TestMakeChallenge(t *testing.T) {
	tests := []struct {
		name       string
		complexity int
		wantErr    bool
	}{
		{"Valid comlexity", 5, false},
		{"Too low complexity", 0, true},
		{"Too high complexity", 10, true},
	}

	cm := ChallengeMaker{}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			challenge, err := cm.MakeChallenge(tt.complexity)
			// check if makeChallenge correctly determine if complexity is in limits
			if (err != nil) != tt.wantErr {
				t.Errorf("makeChallenge() error = %v, wantErr %v", err, tt.wantErr)
			}
			// check if it writes complexity as a first byte
			if err == nil && challenge[0] != strconv.Itoa(tt.complexity)[0] {
				t.Errorf("makeChallenge() first byte = %v, want %v", challenge[0], tt.complexity)
			}
		})
	}
}

func TestCheckSolution(t *testing.T) {
	sc := SolutionChecker{}

	validChallengeComplexity3 := []byte{51, 175, 65, 19, 167, 220, 42, 53, 108, 58, 91, 229, 209, 127, 153, 6, 138, 193, 98, 57, 170, 30, 170, 11, 6, 191, 3, 58, 188, 234, 9, 245, 148}
	// correct solution: []byte{57, 50, 53}
	validSolutionFor1 := []byte{57, 50, 53}
	if !sc.CheckSolution(validChallengeComplexity3, validSolutionFor1) {
		t.Errorf("checkSolution() with valid solution should be true")
	}
	invalidSolutionFor1 := []byte{54, 50, 53}
	if sc.CheckSolution(validChallengeComplexity3, invalidSolutionFor1) {
		t.Errorf("checkSolution() with invalid solution should be false")
	}

	validChallengeComplexity5 := []byte{53, 63, 110, 190, 187, 208, 227, 243, 47, 189, 3, 36, 229, 16, 187, 203, 173, 178, 145, 69, 240, 3, 178, 243, 94, 41, 45, 252, 140, 129, 74, 141, 238}
	// correct solution: []byte{55, 55, 50, 55, 48, 48}
	validSolutionFor2 := []byte{55, 55, 50, 55, 48, 48}
	if !sc.CheckSolution(validChallengeComplexity5, validSolutionFor2) {
		t.Errorf("checkSolution() with valid solution should be true")
	}
	if sc.CheckSolution(validChallengeComplexity5, validSolutionFor1) {
		t.Errorf("checkSolution() with invalid solution should be false")
	}
}

func TestCheckLeadingZeros(t *testing.T) {
	tests := []struct {
		name       string
		hash       []byte
		complexity int
		want       bool
	}{
		{"No leading zeros", []byte{51, 175, 65, 19, 167, 220, 42, 53, 108, 58, 91, 229, 209}, 5, false},
		{"Two leading zeros", []byte{00, 48, 58, 188, 234, 9, 245, 148}, 2, true},
		{"Insufficient zeros", []byte{00, 00, 187, 203, 173, 178, 145, 69, 240}, 5, false},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := checkLeadingZeros(tt.hash, tt.complexity); got != tt.want {
				t.Errorf("checkLeadingZeros() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTrimTrailingZeros(t *testing.T) {
	tests := []struct {
		name string
		data []byte
		want []byte
	}{
		{"No zeroes", []byte{1, 2, 3}, []byte{1, 2, 3}},
		{"Trailing zeroes", []byte{1, 2, 3, 0, 0, 0}, []byte{1, 2, 3}},
		{"Zeroes with last non-zero", []byte{1, 2, 3, 0, 0, 0, 5}, []byte{1, 2, 3, 0, 0, 0, 5}},
		{"All zeroes", []byte{0, 0, 0}, []byte{}},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := trimTrailingZeros(tt.data); !equalSlices(got, tt.want) {
				t.Errorf("trimTrailingZeros() = %v, want %v", got, tt.want)
			}
		})
	}
}

func equalSlices(a, b []byte) bool {
	if len(a) != len(b) {
		return false
	}
	for i := range a {
		if a[i] != b[i] {
			return false
		}
	}
	return true
}
