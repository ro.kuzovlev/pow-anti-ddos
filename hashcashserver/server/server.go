package server

import (
	"ddos/hashcashserver/quotes"
	"fmt"
	"net"
	"sync"
	"time"
)

// Config holds app configuration
type Config struct {
	RequestThreshold int
	TimeoutSeconds   int
	Complexity       int
	Port             string
}

// Server is the server with its configuration and dependencies
type Server struct {
	listener        net.Listener
	config          *Config
	challengeMaker  ChallengeMaker
	solutionChecker SolutionChecker
	requestCount    int
	lastResetTime   time.Time
	complexityMutex sync.Mutex
	wg              sync.WaitGroup
}

// NewServer returns a ready for work server
func NewServer(ln net.Listener, cm ChallengeMaker, sc SolutionChecker, cfg *Config) *Server {
	return &Server{
		listener:        ln,
		config:          cfg,
		challengeMaker:  cm,
		solutionChecker: sc,
	}
}

// HandleConnections runs a server and makesit ready to serve
func (s *Server) HandleConnections() {
	for {
		conn, err := s.listener.Accept()
		if err != nil {
			break
		}
		s.wg.Add(1)
		go s.sendWiseQuote(conn)
	}
}

func (s *Server) sendWiseQuote(conn Conn) {
	defer s.wg.Done()
	defer conn.Close()
	s.adjustComplexity()

	// additional protection from DDOS
	timeoutDuration := time.Duration(s.config.TimeoutSeconds) * time.Second
	conn.SetDeadline(time.Now().Add(timeoutDuration))

	// create a challenge
	challenge, err := s.challengeMaker.MakeChallenge(s.config.Complexity)
	if err != nil {
		fmt.Println("Error making challenge:", err)
		return
	}

	// send the challenge
	_, err = conn.Write(challenge)
	if err != nil {
		fmt.Println("Error sending challenge:", err)
		return
	}

	// receive the solution
	solution := make([]byte, 32)
	_, err = conn.Read(solution)
	if err != nil {
		fmt.Println("Error reading solution from a client:", err)
		return
	}
	// check and process the solution
	if s.solutionChecker.CheckSolution(challenge, solution) {
		fmt.Println("Correct solution.")
		quote := quotes.GetRandomQuote()
		_, err = conn.Write([]byte(quote + "\n"))
		if err != nil {
			fmt.Println("Error sending quote:", err)
			return
		}
	} else {
		fmt.Println("Incorrect solution.")
	}
}

func (s *Server) adjustComplexity() {
	s.complexityMutex.Lock()
	defer s.complexityMutex.Unlock()

	s.requestCount++
	now := time.Now()
	if now.Sub(s.lastResetTime) > time.Second {
		// more than a second passed, reset the count
		s.requestCount = 0
		s.lastResetTime = now
	} else if s.requestCount > s.config.RequestThreshold {
		// too many requests in the last second, increase complexity
		s.config.Complexity++
		s.requestCount = 0
		fmt.Println("Increasing complexity due to high request rate:", s.config.Complexity)
	}
}
