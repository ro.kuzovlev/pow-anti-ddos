package server

import (
	"bytes"
	"fmt"
	"os"
	"strconv"
	"testing"
	"time"
)

// MockConn simulates a network connection
type MockConn struct {
	ReadBuffer  bytes.Buffer
	WriteBuffer bytes.Buffer
	CloseCalled bool
}

func (m *MockConn) Read(b []byte) (n int, err error) {
	return m.ReadBuffer.Read(b)
}

func (m *MockConn) Write(b []byte) (n int, err error) {
	return m.WriteBuffer.Write(b)
}

func (m *MockConn) Close() error {
	m.CloseCalled = true
	return nil
}

// skip for now
func (m *MockConn) SetDeadline(t time.Time) error {
	return nil
}

// MockChallengeMaker simulates challenge creation
type MockChallengeMaker struct {
	Challenge []byte
	Error     error
}

func (mcm *MockChallengeMaker) MakeChallenge(complexity int) ([]byte, error) {
	return mcm.Challenge, mcm.Error
}

// MockSolutionChecker simulates solution checking
type MockSolutionChecker struct {
	Result bool
}

func (msc *MockSolutionChecker) CheckSolution(challenge, solution []byte) bool {
	return msc.Result
}

// TestSendWiseQuote tests the sendWiseQuote function
func TestSendWiseQuote(t *testing.T) {
	mockConn := &MockConn{}
	mockChallengeMaker := &MockChallengeMaker{
		// valid challenge with complexity 3
		Challenge: []byte{51, 175, 65, 19, 167, 220, 42, 53, 108, 58, 91, 229, 209, 127, 153, 6, 138, 193, 98, 57, 170, 30, 170, 11, 6, 191, 3, 58, 188, 234, 9, 245, 148},
	}
	mockSolutionChecker := &MockSolutionChecker{
		Result: true,
	}

	// simulate a valid solution being read
	mockConn.ReadBuffer.Write([]byte{57, 50, 53})

	// doing necessary preparations
	requestThreshold, _ := strconv.Atoi(getEnv("REQUEST_THRESHOLD", "10"))
	timeoutSeconds, _ := strconv.Atoi(getEnv("TIMEOUT_SECONDS", "10"))
	complexity, _ := strconv.Atoi(getEnv("COMPLEXITY", "2"))
	config := Config{
		RequestThreshold: requestThreshold,
		TimeoutSeconds:   timeoutSeconds,
		Complexity:       complexity,
	}
	// nil listener as it's not used in sendWiseQuote
	server := NewServer(nil, mockChallengeMaker, mockSolutionChecker, &config)
	server.wg.Add(1)
	server.sendWiseQuote(mockConn)

	// Wait for sendWiseQuote to finish
	server.wg.Wait()

	// challenge was written to the connection
	challenge := mockConn.WriteBuffer.Next(len(mockChallengeMaker.Challenge))
	if !bytes.Equal(challenge, mockChallengeMaker.Challenge) {
		t.Errorf("First write (challenge) did not match the challenge, got: %v", challenge)
	}

	// Assert that the quote was sent after a correct solution
	quote := mockConn.WriteBuffer.Bytes()
	if !bytes.Contains(quote, []byte("\n")) {
		t.Errorf("Quote should be sent after successful solving of the challenge")
	}
	fmt.Println("Quote: ", string(quote))

	// Assert that the connection was closed
	if !mockConn.CloseCalled {
		t.Error("Conn should be closed")
	}
}

func getEnv(key, defaultValue string) string {
	if value, exists := os.LookupEnv(key); exists {
		return value
	}
	return defaultValue
}
