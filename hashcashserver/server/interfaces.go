package server

import "time"

// Conn holds the necessary methods for a network connection
type Conn interface {
	Read(b []byte) (n int, err error)
	Write(b []byte) (n int, err error)
	Close() error
	SetDeadline(t time.Time) error
}

// ChallengeMaker defines the method for creating a challenge
type ChallengeMaker interface {
	MakeChallenge(complexity int) ([]byte, error)
}

// SolutionChecker defines the method for checking a solution
type SolutionChecker interface {
	CheckSolution(challenge, solution []byte) bool
}
