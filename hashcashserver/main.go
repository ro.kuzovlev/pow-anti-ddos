package main

import (
	"ddos/hashcashserver/hashcash"
	"ddos/hashcashserver/server"
	"fmt"
	"net"
	"os"
	"os/signal"
	"strconv"
	"syscall"

	"github.com/joho/godotenv"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		fmt.Println("Error loading .env")
	}

	// loading env's, making a config
	requestThreshold, _ := strconv.Atoi(getEnv("REQUEST_THRESHOLD", "10"))
	timeoutSeconds, _ := strconv.Atoi(getEnv("TIMEOUT_SECONDS", "10"))
	complexity, _ := strconv.Atoi(getEnv("COMPLEXITY", "2"))
	port := getEnv("PORT", "8080")
	config := server.Config{
		RequestThreshold: requestThreshold,
		TimeoutSeconds:   timeoutSeconds,
		Complexity:       complexity,
		Port:             port,
	}

	listener, err := net.Listen("tcp", ":"+config.Port)
	if err != nil {
		fmt.Println("Error listening:", err.Error())
		os.Exit(1)
	}
	defer listener.Close()
	fmt.Println("Listening on localhost:" + config.Port)

	shutdown := make(chan os.Signal, 1)
	signal.Notify(shutdown, os.Interrupt, syscall.SIGTERM)

	go func() {
		<-shutdown
		fmt.Println("\nGracefully shutting down, please wait for all connections to be closed")
		listener.Close() // stop accepting new connections
	}()

	challengeMaker := hashcash.ChallengeMaker{}
	solutionChecker := hashcash.SolutionChecker{}

	server := server.NewServer(listener, &challengeMaker, &solutionChecker, &config)
	server.HandleConnections()
}

func getEnv(key, defaultValue string) string {
	if value, exists := os.LookupEnv(key); exists {
		return value
	}
	return defaultValue
}
