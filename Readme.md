## How to launch (both):
**Using Docker:**
```
make build
make run
```
**Without Docker:** 
The apps work too by just launching them directly.

## How to test (only for server)
**Using Docker:**
```
make test
```
**Without Docker:** 
go test ./... -v

## Workflow:
- The server starts with a complexity level of 2 (out of 9) and then gradually increases it until the DDoS attack is "mitigated."
- The client attempts to flood the server with requests until it can no longer send more than 10 requests per second.

## Using Hashcash because of:
1. Low computational cost to generate a challenge and check a solution, higher to solve it.
2. Easy variability of complexity (we can fine-tune it).
3. Limited time to implement + limited information about potential threats (device types, amount, nature of an attack, etc), leading to the choice of a universal solution.

## Further considerations:
1. Increase variability of complexity by checking not for an amount of a leading zeroes ("0"), but, maybe, for an amount of a leading numbers ("0-9"); because now we have too steep complexity increase between 4 and 7.
2. Implement heuristics-based complexity change for individual clients (esp. those with high activity) to not penalize all clients while under DDOS.
3. "checkLeadingZeros" function in server should be rewritten to work directly with []byte w/o converting to a string to save cpu resources of a server.
4. In case of a smaller number of attackers with high-tier optimized for that Hashcash machines (eg ASICs) it would be necessary to either implement p. 2 or add memory-bound PoW function with efficient verification (to not make server do the same amount of work as a client) or do another types of defence.